package ru.mail.course;

import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.JDBCLoginService;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.filter.OnlyOnePostRequestFilter;
import ru.mail.course.servlet.ProductServlet;

import javax.servlet.DispatcherType;
import java.net.URL;
import java.util.EnumSet;

public final class Application {
    public static void main(@NotNull String[] args) throws Exception {
        final Server server = new Server();

        final HttpConfiguration httpConfig = new HttpConfiguration();

        final HttpConnectionFactory httpConnectionFactory = new HttpConnectionFactory(httpConfig);

        final ServerConnector serverConnector = new ServerConnector(server, httpConnectionFactory);

        serverConnector.setHost("localhost");
        serverConnector.setPort(3466);

        server.setConnectors(new Connector[]{serverConnector});

        final ContextHandlerCollection contextHandlerCollection = new ContextHandlerCollection();

        ServletContextHandler helpInfoContext = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        helpInfoContext.setContextPath("/help");
        final URL resource = Application.class.getResource("/static");
        helpInfoContext.setBaseResource(Resource.newResource(resource.toExternalForm()));
        helpInfoContext.setWelcomeFiles(new String[]{"help-info-page.html"});
        helpInfoContext.addServlet(new ServletHolder("default", DefaultServlet.class), "/");

        contextHandlerCollection.addHandler(helpInfoContext);

        ServletContextHandler productServletContext = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        productServletContext.setContextPath("/");
        productServletContext.addServlet(new ServletHolder("default", new ProductServlet()), "/product");

        final OnlyOnePostRequestFilter filter = new OnlyOnePostRequestFilter();
        final FilterHolder filterHolder = new FilterHolder(filter);
        productServletContext.addFilter(filterHolder, "/*", EnumSet.of(DispatcherType.REQUEST));

        contextHandlerCollection.addHandler(productServletContext);

        final String jdbc_config = Application.class.getResource("/jdbc_config").toExternalForm();
        final JDBCLoginService jdbcLoginService = new JDBCLoginService("login", jdbc_config);
        final ConstraintSecurityHandler securityHandler = new SecurityHandlerBuilder().build(server, jdbcLoginService);

        securityHandler.setHandler(contextHandlerCollection);

        server.setHandler(securityHandler);
        server.start();
    }
}
