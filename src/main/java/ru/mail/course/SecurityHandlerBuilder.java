package ru.mail.course;

import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.security.Constraint;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;

public final class SecurityHandlerBuilder {
    private static final @NotNull String ROLE_MANAGER = "manager";
    private static final @NotNull String ROLE_GUEST = "guest";

    private final @NotNull ConstraintSecurityHandler security = new ConstraintSecurityHandler();

    public final @NotNull ConstraintSecurityHandler build(@NotNull Server server,
                                                          @NotNull LoginService loginService) {
        server.addBean(loginService);
        security.setLoginService(loginService);

        final List<ConstraintMapping> constraintMappings = new ArrayList<>();

        constraintMappings.addAll(constraintPostMapping(
                buildConstraint(ROLE_MANAGER),
                Collections.singletonList("/product")
        ));
        constraintMappings.addAll(constraintGetMapping(
                buildConstraint(ROLE_GUEST, ROLE_MANAGER),
                Arrays.asList("/help", "/product")
        ));

        security.setConstraintMappings(constraintMappings);
        security.setAuthenticator(new BasicAuthenticator());
        security.setDenyUncoveredHttpMethods(true);
        return security;
    }

    private static @NotNull Constraint buildConstraint(@NotNull String... userRoles) {
        final Constraint starterConstraint = new Constraint();
        starterConstraint.setName(Constraint.__BASIC_AUTH);
        starterConstraint.setRoles(userRoles);
        starterConstraint.setAuthenticate(true);
        return starterConstraint;
    }

    private static @NotNull Collection<ConstraintMapping> constraintFullMapping(@NotNull Constraint constraint,
                                                                                @NotNull Collection<String> paths) {
        return constraintMapping(constraint, paths, "*");
    }

    protected static @NotNull Collection<ConstraintMapping> constraintGetMapping(@NotNull Constraint constraint,
                                                                                 @NotNull Collection<String> paths) {
        return constraintMapping(constraint, paths, "GET");
    }

    protected static @NotNull Collection<ConstraintMapping> constraintPostMapping(@NotNull Constraint constraint,
                                                                                 @NotNull Collection<String> paths) {
        return constraintMapping(constraint, paths, "POST");
    }

    private static @NotNull Collection<ConstraintMapping> constraintMapping(@NotNull Constraint constraint,
                                                                            @NotNull Collection<String> paths,
                                                                            @NotNull String method) {
        return paths.stream()
                .map(path -> {
                            final ConstraintMapping mapping = new ConstraintMapping();
                            mapping.setConstraint(constraint);
                            mapping.setPathSpec(path);
                            mapping.setMethod(method);
                            return mapping;
                        }
                ).collect(Collectors.toList());
    }
}
