package ru.mail.course.filter;

import org.jetbrains.annotations.NotNull;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Фильтр пропускает только один POST запрос.
 */
public final class OnlyOnePostRequestFilter implements Filter {
    @SuppressWarnings("NullableProblems")
    @NotNull
    private FilterConfig filterConfig;

    private int count = 0;

    @Override
    public void init(@NotNull FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(@NotNull ServletRequest request,
                         @NotNull ServletResponse response,
                         @NotNull FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        if (httpServletRequest.getMethod().equalsIgnoreCase("POST")) {
            if (++count <= 1) {
                chain.doFilter(request, response);
            } else {
                ((HttpServletResponse)response).sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
