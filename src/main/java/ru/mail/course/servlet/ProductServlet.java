package ru.mail.course.servlet;

import com.google.gson.Gson;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.dao.DaoFactory;
import ru.mail.course.dao.ProductDao;
import ru.mail.course.dto.Product;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Сервлет для обработки запросов, связанных с сущностью {@link Product}.
 */
public final class ProductServlet extends HttpServlet {
    @NotNull
    private ProductDao productDao = DaoFactory.getProductDao();

    @Override
    protected void doGet(@NotNull HttpServletRequest req, @NotNull HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            List<Product> products = productDao.getAll();
            String productsJson = new Gson().toJson(products);
            resp.getWriter().println(productsJson);
        } catch (SQLException e) {
            e.printStackTrace();
            resp.getWriter().println(Arrays.toString(e.getStackTrace()));
        } finally {
            resp.getWriter().close();
        }
    }

    @Override
    protected void doPost(@NotNull HttpServletRequest req, @NotNull HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String productJson = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        Product product = new Gson().fromJson(productJson, Product.class);
        try {
            productDao.create(product);
        } catch (SQLException e) {
            e.printStackTrace();
            resp.getWriter().println(Arrays.toString(e.getStackTrace()));
        } finally {
            resp.getWriter().close();
        }
    }
}
