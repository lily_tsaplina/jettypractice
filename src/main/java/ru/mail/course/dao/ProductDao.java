package ru.mail.course.dao;

import org.jetbrains.annotations.NotNull;
import ru.mail.course.dto.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с БД для сущности Товар ({@link Product}).
 */
public final class ProductDao {
    @NotNull
    Connection connection;

    public ProductDao(@NotNull Connection connection) {
        this.connection = connection;
    }

    {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/products",
                    "postgres",
                    "blossom"
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    public List<Product> getAll() throws SQLException {
        List<Product> allProducts = new ArrayList<>();
        String query = "SELECT * FROM Product";
        ResultSet resultSet;
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            resultSet = statement.executeQuery();
            while(resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String manufacturer = resultSet.getString("manufacturer");
                int count = resultSet.getInt("count");
                allProducts.add(new Product(id, name, manufacturer, count));
            }
        }
        return allProducts;
    }

    public void create(@NotNull Product product) throws SQLException {
        String id;
        if (product.getId() != null) {
            id = product.getId().toString();
        } else {
            id = "nextval('product_id_seq')";
        }
        String query = "INSERT INTO Product (id, name, manufacturer, count) VALUES(" + id + ", ?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, product.getName());
            statement.setString(2, product.getManufacturer());
            statement.setInt(3, product.getCount());
            statement.executeUpdate();
        }
    }
}
