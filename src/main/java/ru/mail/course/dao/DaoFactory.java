package ru.mail.course.dao;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class DaoFactory {
    @Nullable
    private static volatile ProductDao productDao;

    @SuppressWarnings("NullableProblems")
    @NotNull
    private static Connection connection;

    static {
        try {
            connection = DriverManager.getConnection(
                        "jdbc:postgresql://127.0.0.1:5432/products",
                        "postgres",
                        "blossom"
                );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("ConstantConditions")
    @NotNull
    public static ProductDao getProductDao() {
        if (productDao == null) {
            productDao = new ProductDao(connection);
        }
        return productDao;
    }
}
