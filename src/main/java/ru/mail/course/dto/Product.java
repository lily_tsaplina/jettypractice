package ru.mail.course.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * DTO для сущности Товар.
 */
@Data
@AllArgsConstructor
public final class Product {
    @Nullable
    Integer id;
    @NotNull
    String name;
    @NotNull
    String manufacturer;
    int count;
}
