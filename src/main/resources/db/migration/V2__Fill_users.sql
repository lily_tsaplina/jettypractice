INSERT INTO person(id, name, password)
VALUES(1, 'manager', 'manager');
INSERT INTO person(id, name, password)
VALUES(2, 'guest', 'guest');

INSERT INTO role(id, name)
VALUES(1, 'manager');
INSERT INTO role(id, name)
VALUES(2, 'guest');

INSERT INTO person_roles(id, person_id, role_id)
VALUES(1, 1, 1);
INSERT INTO person_roles(id, person_id, role_id)
VALUES(2, 2, 2);